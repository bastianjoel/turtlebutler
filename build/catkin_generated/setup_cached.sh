#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/bastian/ros_workspace/sandbox/turtlebot_butler/build/devel:$CMAKE_PREFIX_PATH"
export ROS_PACKAGE_PATH="/home/bastian/ros_workspace/sandbox/turtlebot_butler:/opt/ros/indigo/share:/opt/ros/indigo/stacks"