#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

using namespace std;

class OrderHandler {
	private:
		sql::Driver* driver;
		std::auto_ptr<sql::Connection> con;
		typedef struct {
			int id;
			int status;
			float posx;
			float posy;
		} order;
		vector<order> orders;
		void updateOrderStatus(int id, int status) {
			std::auto_ptr<sql::PreparedStatement> stmt(con->prepareStatement("UPDATE orders SET status=? WHERE id=?;"));
			stmt->setInt(1, status);
			stmt->setInt(2, id);
			stmt->executeUpdate();
			if(status > 0) {
				for(unsigned i = 0; i < orders.size(); i++) {
					if(orders[i].id == id) orders.erase(orders.begin() + i);
				}
			} else {
				updateOrders();
			}
		}
	public:
		OrderHandler(const string url, const string user, const string pass, const string database) {
			try {
				driver = get_driver_instance();
				con = std::auto_ptr<sql::Connection>(driver->connect(url, user, pass));
				con->setSchema(database);
				cout << "MySQL connected successful..." << endl;
				updateOrders();
			} catch (sql::SQLException &e) {
				cout << "# ERR: SQLException in " << __FILE__;
				cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
				cout << "# ERR: " << e.what();
				cout << " (MySQL error code: " << e.getErrorCode();
				cout << ", SQLState: " << e.getSQLState() << " )" << endl;
			}
		}

		void updateOrders() {
			orders.erase(orders.begin(), orders.end());
			std::auto_ptr<sql::Statement> stmt(con->createStatement());

			stmt->execute("SELECT o.id, status, posx, posy FROM orders as o, tables as t WHERE o.tableId = t.id AND status='0' ORDER BY updated_at;");
			std::auto_ptr< sql::ResultSet > res;
			do {
				res.reset(stmt->getResultSet());
				while (res->next()) {
					orders.push_back((order) { res->getInt("id"), res->getInt("status"), (float) res->getDouble("posx"), (float) res->getDouble("posy") });
				}
			} while (stmt->getMoreResults());
		}

		int numOrders() {
			return orders.size();
		}

		order getCurrentOrder() {
			order result;

			if(!orders.empty()) {
				int currentId = orders.front().id;

				std::auto_ptr<sql::PreparedStatement> stmt(
					con->prepareStatement("SELECT o.id, status, posx, posy FROM orders as o, tables as t WHERE o.tableId=t.id AND o.id=?;")
				);
				stmt->setInt(1, currentId);
				stmt->execute();
				std::auto_ptr< sql::ResultSet > res;
				do {
					res.reset(stmt->getResultSet());
					while (res->next()) {
						result = (order) { 
							res->getInt("id"), 
							res->getInt("status"), 
							(float) res->getDouble("posx"), 
							(float) res->getDouble("posy") 
						};
					}
				} while (stmt->getMoreResults());
				//result = orders.front();
			}

			return result;
		}

		order getOrder(int id) {
			order result;

			int currentId = id;

			std::auto_ptr<sql::PreparedStatement> stmt(
				con->prepareStatement("SELECT o.id, status, posx, posy FROM orders as o, tables as t WHERE o.tableId=t.id AND o.id=?;")
			);
			stmt->setInt(1, currentId);
			stmt->execute();
			std::auto_ptr< sql::ResultSet > res;
			do {
				res.reset(stmt->getResultSet());
				while (res->next()) {
					result = (order) { 
						res->getInt("id"), 
						res->getInt("status"), 
						(float) res->getDouble("posx"), 
						(float) res->getDouble("posy") 
					};
				}
			} while (stmt->getMoreResults());
			
			return result;
		}

		void acceptOrder(int id) {
			updateOrderStatus(id, 1);
		}

		void setArrived(int id) {
			updateOrderStatus(id, 2);
		}

		void finishOrder(int id) {
			updateOrderStatus(id, 3);
		}

		void resetOrder(int id) {
			updateOrderStatus(id, 0);
		}
};
