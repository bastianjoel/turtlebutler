#include <ros/ros.h>

#include <kobuki_msgs/SensorState.h>
#include <smart_battery_msgs/SmartBatteryStatus.h>

#define MAX_CHARGE 160

using namespace std;

class BatteryHandler {
	private:
		ros::NodeHandle _nh;
		ros::Subscriber  laptop_sub;
		ros::Subscriber  kobuki_sub;
		void laptopCallback(const smart_battery_msgs::SmartBatteryStatusConstPtr msg) {
			//laptopBatteryState = 0;
			laptopBatteryState = msg->percentage;
		}
		void kobukiCallback(const kobuki_msgs::SensorStateConstPtr msg) {
			//kobukiBatteryState = 0;
			kobukiBatteryState = msg->battery / MAX_CHARGE * 100;
			charging = (msg->charger != 0);
		}
	public:
		int kobukiBatteryState;
		int laptopBatteryState;
		bool charging;

		BatteryHandler(ros::NodeHandle &nh) {
			_nh = nh;
			charging = false;
			kobuki_sub = _nh.subscribe("/mobile_base/sensors/core", 1, &BatteryHandler::kobukiCallback, this);
			laptop_sub = _nh.subscribe("/laptop_charge/", 1, &BatteryHandler::laptopCallback, this);

			kobukiBatteryState = 0;
			laptopBatteryState = 0;
		}
};
