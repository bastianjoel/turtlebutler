#include "BatteryHandler.cpp"

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <kobuki_msgs/AutoDockingAction.h>
#include <kobuki_msgs/AutoDockingGoal.h>
#include <actionlib/client/simple_action_client.h>

using namespace std;

class Station {
	private:
		ros::NodeHandle _nh;
		ros::Publisher vel_pub;
		actionlib::SimpleActionClient<kobuki_msgs::AutoDockingAction>* base_pub;
		BatteryHandler* battery;
		bool isOnStation() {
			return battery->charging;
		}
	public:
		Station(ros::NodeHandle nh) {
			_nh = nh;
			battery = new BatteryHandler(_nh);
			vel_pub = _nh.advertise<geometry_msgs::Twist>("cmd_vel_mux/input/navi", 1);
			base_pub = new actionlib::SimpleActionClient<kobuki_msgs::AutoDockingAction>("/dock_drive_action", true);
			base_pub->waitForServer();
		}
		void prepareMovement() {
			if(isOnStation()) {
				geometry_msgs::Twist base_cmd;
				base_cmd.linear.x = -0.15;
				base_cmd.angular.z = 0;//Set the loop rate (Hz)
				ros::Rate loop_rate(10);
				int count = 0;

				//After pressing CTRL + C, "nh_.ok()" will return "false"
				while (_nh.ok() && count < 20)
	  			{
					//publish the assembled command
					vel_pub.publish(base_cmd);

					ros::spinOnce();
					count++;

	    			loop_rate.sleep();
				}
			}
		}
		void enterStation() {
			if(!isOnStation()) {
				kobuki_msgs::AutoDockingGoal goal;

				base_pub->sendGoal(goal);

				base_pub->waitForResult(ros::Duration(30.0));
			}
		}
};
