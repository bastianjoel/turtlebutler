//ROS HAUPT BIBLIOTHEK EINBINDEN
#include <ros/ros.h>

//KPBUKI_MSGS BIBLIOTHEKEN EINBINDEN
#include <kobuki_msgs/BumperEvent.h>

#include "BeepOut.cpp"
#include "Blink.cpp"

#include "getch.cpp"

//KLASSE FÜR WALLHIT (WENN BUMPER GEDRÜCKT WIRD) DEFINIEREN
class WallHit {
	//PRIVATER TEIL DER KLASSE (FÜR ANDERE KLASSEN UND FUNKTIONEN
	//UNSICHTBAR)
	private:

		//DER KOMMUNIKATIONSPUNKT DER KLASSE
		ros::NodeHandle _nh;

		//DER "EINSCHREIBER" (NACHRICHTENSAMMLER) DER KLASSE
		ros::Subscriber bumperSubscriber;

		//FUNKTION FÜR STÄNDIGES ABFRAGEN VON WERTEN (CALLBACKS
		//BENÖTIGT GERÜEST DES NACHRICHTEN TYPES UNUD EINE VARIABLE ZUM SPEICHERN
		void callback(const kobuki_msgs::BumperEventConstPtr bumpers)
		{

			//VARIABLEN AN ÖFFENTLICHEN TEIL ÜBERGEBEN
			isHit = (bumpers->state==1);

			//WENN BUMPER GEDRUECKT
			if(isHit)
			{

				//WARNUNG AUSGEBEN
				ROS_INFO("HIT THE [W]ALL / [E]XIT PRG ? : ");


				//OBJEKTE FÜR LEDS UND TON ERSTELLEN
				Blink lamp(_nh);
				BeepOut peep(_nh);

				//TON ERZEUGEN UND LEDS LEUHCHTEN LASSEN
				peep.beep(2,3);
				lamp.blink(2,1,1);
				lamp.blink(2,1,0);


				//FUNKTION FÜR TASTENEINGABE
				int c = getch();   // call your non-blocking input function

				//SWITCH ZUM AUSWERTEN DER EINGABE
				switch(c)
				{
					//WENN W
					case 'w':

						//LED LEUCHTET
						lamp.blink(1,400,0);
						//MELDUNG AUSGEBEN
						ROS_INFO("RESCUED FROM WALL");
						//LED GEHT AUS
						lamp.blink(0,1,0);
						//AUS SWITCH HERRAUS
						break;

					//WENN E
					 case 'e':

						 //TON AUSGEBEN
						 peep.beep(1,1);
						 //LED EUCHTET VOM TYP 3
						 lamp.blink(3,400,0);
						 //INFO FÜR BENUTZER
						 ROS_INFO("PROGR WILL HALT");
						 //LEDS GEHEN AUS
						 lamp.blink(0,1,0);
						 lamp.blink(0,1,1);
						//ROS SYSTEM WIRD ANGEHALTEN
						ros::shutdown();
						//AUS SWITCH AUSBRECHEN
						break;

					//WENN WEDER W NOCH E
					 default:
						 //FEHLER MELDUNG ausgeben
						 ROS_INFO("WRONG CHAR KEEP RUNNING");
						 //AUS SWITCH AUSBRECHEN
						 break;
				 }
			}


		}


		//OEFFENTLICHER TEIL DER KLASSE (FÜR ANDERE KLASSEN UND FUNKTIONEN
		//SICHTBAR
	public:
		//KLASSEN INTERNE FUNNKTION ZUM DEFINEIREN DES HANDLER IN ABHÄNGIGKEIT DES
		//KLASSEN INTERNEN KOMMUNIKATIONS PUNKTES
		WallHit(ros::NodeHandle &nh) {
			//BUMPER IST GEDRÜCKT WIRD AUS FALSCH GESETZT
			isHit=false;

			//KLASSEN INTERNER KOMMUNIKATIONSPUNKT WIRD MIT EXTERNEM GLEICH GESETZT
			_nh= nh;

			//SUBSCRIBER WIRD DEFINIERT
			//BENÖTIGT TOPIC ZUM EINSCHREIBEN, FREQUENZ ZUM NACHFRAGEN, FUNKTION ZUM ABFRAGEN , OBJEKT ZUM SPEICHERN
			//(DA INTERN DER KLASSE EINFACH THIS FUKTINO)
			bumperSubscriber = _nh.subscribe("/mobile_base/events/bumper", 100,  &WallHit::callback, this);
		}
		//BOOL VARIABLE FÜR SPÄTER WIRD GESETZT
		bool isHit;
};
