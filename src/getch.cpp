//C++ STANDARDT BIBLIOTHEKEN EINBINDEN
#include <iostream>
#include <ostream>

//FÜR GETCH() FUNKTION
#include <termios.h>

// GETCH() FUNKTION FÜR TASTEINEINGABE DEFINIEREN
int getch()
{
	static struct termios oldt, newt;
	tcgetattr( STDIN_FILENO, &oldt);           // SPEICHERT ALTEN STANDT
	newt = oldt;
	newt.c_lflag &= ~(ICANON);                 // DEAKTIVIERT BUFFERING
	tcsetattr( STDIN_FILENO, TCSANOW, &newt);  // WENDET NEUE SETTINGS AN

	int c = getchar();  // EINGABE LESEN

	tcsetattr( STDIN_FILENO, TCSANOW, &oldt);  //ALTEN STANDT WIEDERHERSTELLEN
	return c;
}
