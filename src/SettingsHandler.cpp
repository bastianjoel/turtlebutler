#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

using namespace std;

class SettingsHandler {
	private:
		sql::Driver* driver;
		std::auto_ptr<sql::Connection> con;
		typedef struct {
			float x;
			float y;
		} coord;
		float getProperty(std::string name) {
			std::auto_ptr<sql::PreparedStatement> stmt(con->prepareStatement("SELECT value FROM settings WHERE name = ?;"));
			stmt->setString(1, name);

			stmt->execute();
			std::auto_ptr< sql::ResultSet > res;
			do {
				res.reset(stmt->getResultSet());
				while (res->next()) {
					return (float) res->getDouble("value");
				}
			} while (stmt->getMoreResults());
		}
	public:
		SettingsHandler(const string url, const string user, const string pass, const string database) {
			try {
				driver = get_driver_instance();
				con = std::auto_ptr<sql::Connection>(driver->connect(url, user, pass));
				con->setSchema(database);
				cout << "MySQL connected successful..." << endl;
			} catch (sql::SQLException &e) {
				cout << "# ERR: SQLException in " << __FILE__;
				cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
				cout << "# ERR: " << e.what();
				cout << " (MySQL error code: " << e.getErrorCode();
				cout << ", SQLState: " << e.getSQLState() << " )" << endl;
			}
		}

		coord getBaseCoord() {
			coord baseCoord;
			baseCoord.x = getProperty("basex");
			baseCoord.y = getProperty("basey");
			return baseCoord;
		}
};
