//ROS HAUPT BIBLIOTHEK EINBINDEN
#include <ros/ros.h>

//KPBUKI_MSGS BIBLIOTHEKEN EINBINDEN
#include <kobuki_msgs/ButtonEvent.h>

#include "Cliff.cpp"
#include "BeepOut.cpp"

//KLASSE FÜRPRESSBUTTON (WENN BUTTON AUF DER TURTLE GEDRÜCK WIRD) DEFINIEREN
class PressButton
{
	//PRIVATER TEIL DER KLASSE (FÜR ANDERE KLASSEN UND FUNKTIONEN
	//UNSICHTBAR)
	private:
		//DER KOMMUNIKATIONSPUNKT DER KLASSE
		ros::NodeHandle _nh;

		//DER "EINSCHREIBER" (NACHRICHTENSAMMLER) DER KLASSE
		ros::Subscriber BtnSubscriber;

		//FUNKTION FÜR STÄNDIGES ABFRAGEN VON WERTEN (CALLBACKS)
		//BENÖTIGT GERÜEST DES NACHRICHTEN TYPES UNUD EINE VARIABLE ZUM SPEICHERN
		void callback(const kobuki_msgs::ButtonEventConstPtr _btn)
		{
			//VARIABLEN AN ÖFFENTLICHEN TEIL ÜBERGEBEN
			_btn_nr = _btn->button;
			_btn_state = (_btn->state==1);


			//WENN EIN KNOPF GEDRÜCKT
			if(_btn_state)
			{
				//OBJEKT FÜR CLIFF (ENTFERNUGN ZUM BODEN)
				Cliff cliffdr(_nh);

				//OBJEKTE FÜR LEDS UND TON ERSTELLEN
				Blink lamp(_nh);
				BeepOut peep(_nh);

				//SWITCH ZUR AUSWERTUNG DER BUTTONS
				switch (_btn_nr)
				{
					//WENN KNOPF 0
					case 0:
						//TON WIRD ERZEUGT
						peep.beep(6,2);
						//INFO FÜR BENUTZER
						ROS_INFO("TO GROUND : %i",cliffdr.dist);
						//BRICHT AUS SWITCH AUS
						break;
					//WENN KNOPF 1
					case 1:
						//ERZEUGT TON
						peep.beep(2,2);
						//BRICHT AUS SWITCH AUS
						break;
					//WENN KNOPF 2
					case 2:
						//WARNUNG AUSGEBEN
						ROS_INFO("!!!!!!!!!!!!!SYSTEM SOS SHUTDOWN !!!!!!!!!!!!!!!!");
						//TON ERZEUGEN
						peep.beep(1,1);
						//LAMPEN LEUCHTEN UND GEHEN AUS
						 lamp.blink(3,400,0);
						 lamp.blink(3,400,1);
						 lamp.blink(0,1,1);
						 lamp.blink(0,1,0);
						 //ROS SYSTEM WIRD ANGEHALTEN
						 ros::shutdown();
				};
			}
		}

	//OEFFENTLICHER TEIL DER KLASSE (FÜR ANDERE KLASSEN UND FUNKTIONEN
	//SICHTBAR
	public:
		//KLASSEN INTERNE FUNNKTION ZUM DEFINEIREN DES HANDLER IN ABHÄNGIGKEIT DES
		//KLASSEN INTERNEN KOMMUNIKATIONS PUNKTES
		PressButton(ros::NodeHandle &nh)
		{
			//KLASSEN INTERNER KOMMUNIKATIONSPUNKT WIRD MIT EXTERNEM GLEICH GESETZT
			_nh= nh;

			_btn_nr = 0;
			_btn_state = false;

			//SUBSCRIBER WIRD DEFINIERT
			//BENÖTIGT TOPIC ZUM EINSCHREIBEN, FREQUENZ ZUM NACHFRAGEN, FUNKTION ZUM ABFRAGEN , OBJEKT ZUM SPEICHERN
			//(DA INTERN DER KLASSE EINFACH THIS FUKTINO)
			BtnSubscriber = _nh.subscribe("/mobile_base/events/button", 10,  &PressButton::callback, this);
		}

		//ÖFFENTLICHE VARIABLE FÜR NUMMER DES KNOPFES
		int _btn_nr;

		//BOOL VARIABLE OB KNOPF GEDRÜCKT ODER NICHT
		bool _btn_state;
};
