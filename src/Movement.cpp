#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <ros/ros.h>

#include <move_base_msgs/MoveBaseAction.h>
#include <move_base_msgs/MoveBaseGoal.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/simple_goal_state.h>

#include "Station.cpp"

using namespace std;

class Movement {
	private:
		ros::NodeHandle _nh;
		actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction>* client;
		Station* station;
		typedef struct {
			float x;
			float y;
		} coord;
		coord base;

	public:
		Movement (ros::NodeHandle &nh, float basex, float basey) {
			_nh = nh;
			client = new actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction>("move_base", true);
			client->waitForServer();
			station = new Station(_nh);
			base.x = basex;
			base.y = basey;
		}

		void moveTo(float posx, float posy) {
			station->prepareMovement();
			geometry_msgs::Point point;
			point.x = posx; point.y = posy; point.z = 0;

			geometry_msgs::Quaternion quaternion;
			quaternion.x = 0; quaternion.y = 0;
			quaternion.z = 0.892; quaternion.w = -1.500;

			move_base_msgs::MoveBaseGoal goal;

			goal.target_pose.header.frame_id = "map";
			goal.target_pose.header.stamp = ros::Time::now();
			geometry_msgs::Pose pose;
			pose.position = point;
			pose.orientation = quaternion;
			goal.target_pose.pose = pose;

			client->sendGoal(goal);
		}

		void moveTo(coord pos) {
			moveTo(pos.x, pos.y);
		}

		bool hasArrived() {
			 return client->getState() == actionlib::SimpleClientGoalState::SUCCEEDED;
		}

		void moveOnStation() {
			moveTo(base);
			ros::Rate loop_rate(10);
			while(_nh.ok() && !hasArrived()) { //Wait until all movements finished
				loop_rate.sleep();
			}
			station->enterStation();
		}
};
