//ROS HAUPT BIBLIOTHEK EINBINDEN
#include <ros/ros.h>

//KPBUKI_MSGS BIBLIOTHEKEN EINBINDEN
#include <kobuki_msgs/WheelDropEvent.h>

#include "getch.cpp"

#include "Blink.cpp"
#include "BeepOut.cpp"

//KLASSE FÜR WHEELDROP (WENN RAD NACH UNTEN HÄNGT) DEFINIEREN
class WheelDrop
{

	//PRIVATER TEIL DER KLASSE (FÜR ANDERE KLASSEN UND FUNKTIONEN
	//UNSICHTBAR)
	private:

		//DER KOMMUNIKATIONSPUNKT DER KLASSE
		ros::NodeHandle _nh;

		//DER "EINSCHREIBER" (NACHRICHTENSAMMLER) DER KLASSE
		ros::Subscriber DropSub;


		//FUNKTION FÜR STÄNDIGES ABFRAGEN VON WERTEN (CALLBACKS
		//BENÖTIGT GERÜEST DES NACHRICHTEN TYPES UNUD EINE VARIABLE ZUM SPEICHERN
		void callback(const kobuki_msgs::WheelDropEventConstPtr _drop)
		{
			//VARIABLEN AN ÖFFENTLICHEN TEIL ÜBERGEBEN
			isDrop =(_drop->state==1);
			side =(_drop->wheel==1);


			//WENN BUMPER GEDRÜCKT
			if(isDrop)
			{
				//OBJEKT VOM TYP BLINK ZUM BLINKEN DER LEDS
				//IN ABHÄNGIGKEIT DES KLASSEN INTERNEN KOMMUNIKATIONSPUNKTES
				Blink lamp(_nh);

				//OBJEKT VOM TYP BLINK ZUM TON AUSGEBEN
				//IN ABHÄNGIGKEIT DES KLASSEN INTERNEN KOMMUNIKATIONSPUNKTES
				BeepOut peep(_nh);

				//VARIABLE FÜR MANUELLE RETTUNG
				bool _resc=false;

				//TON VOM TYP 3 5 MAL AUSGEBEN
				peep.beep(5,3);

				//LED 1 BLINKT VOM TYP 2
				lamp.blink(2,1,1);
				//LED 2 BLINKT VOM TYP 2
				lamp.blink(2,1,0);


				//TERMINAL AUSGABE
				ROS_INFO("ERROR : WHEELDROP");

				//WENN SEITE RECHTS (0) IST
				if(side) {
					//AUSGEBEN VON BITTE
					ROS_INFO("FIX WHEEL : RIGHT");

				//WENN SEITE LINKS (1) IST
				} else {
					//AUSGEBEN VON BITTE
					ROS_INFO("FIX WHEEL : LEFT");
				}

				//TERMINAL AUSGABE MIT AUFFORDERUNG
				ROS_INFO("[R]ESCUED ??? :");

				//SOLANGE NOCH NICHT MANUELL GERETTET
				while(!_resc)
				{

					//FUNKTION FÜR TASTENEINGABE
					int c = getch();

					//SWITCH FÜR EINGABE
					switch(c)
					{
						//WENN R
						case 'r':

							//WENN NICHTMEHR UNTEN
							if(!isDrop)
							{
								//LED 1 BLINKT VOM TYP 1
								lamp.blink(1,200,0);
								//LED 2 BLINKT VOM TYP2
								lamp.blink(2,1,1);

								//VARIABLE (MANUELLE RETTUNG) WIRD AUF WAHR GESETZT
								_resc=true;

								//WENN RAD NOCH RAUS
							}else
							{

								//LED 1 BLINKT VOM TYP 2
								lamp.blink(2,1,0);

								//WENN SEITE RECHTS (0)
								if(side)
								{
									//WARNUNG AUSGEBEN
									ROS_INFO("FIX WHEEL : RIGHT");

								//WENN SEITE LINKS (1)
								}else
								{
									//WARNUNG AUSGEBEN
									ROS_INFO("FIX WHEEL : LEFT");
								}
							}

							//AM ENDE DES CASE IMMER BREAK ZUM VERLASSEN DES SWITCHES
							break;

							//DEAFULT WERT WENN EINGABE NICHT R
						default :
							//LED 1 BLINNKT VOM TYP 3
							lamp.blink(3,400,0);
							//LED 2 BLINKT VOM TYP 2
							lamp.blink(2,400,1);

							//WARNUNG AUSGEBEN
							ROS_INFO("WRONG CHAR !!");

							//LAMPE 1 SCHALTET AUS
							lamp.blink(0,1,0);
							//VERLASSEN DES SWITCHES
							break;
					};
				}
			}
		}

	//OEFFENTLICHER TEIL DER KLASSE (FÜR ANDERE KLASSEN UND FUNKTIONEN
	//SICHTBAR
	public:

		//BOOL VARIABLEN FÜR RAD RUNTER (ISDROP) UND WELHCE SEITE (SIDE)
		bool isDrop, side;

		//KLASSEN INTERNE FUNNKTION ZUM DEFINEIREN DES HANDLER IN ABHÄNGIGKEIT DES
		//KLASSEN INTERNEN KOMMUNIKATIONS PUNKTES
		WheelDrop(ros::NodeHandle &nh)
		{
			//RAD RUNTER WIRD AUF FALSCH GESETZT
			isDrop=false;
			side=false;

			//KLASSEN INTERNER KOMMUNIKATIONSPUNKT WIRD MIT EXTERNEM GLEICH GESETZT
			_nh= nh;

			//SUBSCRIBER WIRD DEFINIERT
			//BENÖTIGT TOPIC ZUM EINSCHREIBEN, FREQUENZ ZUM NACHFRAGEN, FUNKTION ZUM ABFRAGEN , OBJEKT ZUM SPEICHERN
			//(DA INTERN DER KLASSE EINFACH THIS FUKTINO)
			DropSub = _nh.subscribe("/mobile_base/events/wheel_drop", 1,  &WheelDrop::callback, this);
		}


};
