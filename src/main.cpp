#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <ros/ros.h>

#include "OrderHandler.cpp"
#include "Movement.cpp"
//Used in Movement.cpp: #include "Station.cpp"
//						#include "BatteryHandler.cpp"
#include "Blink.cpp"
#include "SettingsHandler.cpp"


#define MYSQL_HOST "localhost"
#define MYSQL_USER "worlduser"
#define MYSQL_PASS "worldpass"
#define MYSQL_DB "turtlebot"

using namespace std;

int main(int argc, char** argv) {
	//init the ROS node
	ros::init(argc, argv, "turtlebot");
	ros::NodeHandle nh;

	//Send log message, how to stop
	ROS_INFO("Press CTRL + C to stop");

  	OrderHandler connection(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
  	SettingsHandler settings(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB);
  	Movement movement(nh, settings.getBaseCoord().x, settings.getBaseCoord().y);

  	bool orderInProgress = false, waiting = false;
  	int currentOrderId;
  	int timer = 0;

  	Blink LedStat(nh);

  	LedStat.blink(1,400,0);
	LedStat.blink(1,400,1);
	LedStat.blink(2,400,0);
	LedStat.blink(2,400,1);
	LedStat.blink(3,400,0);
	LedStat.blink(3,400,1);
	LedStat.blink(0,400,0);
	LedStat.blink(0,400,1);

	//Set the loop rate (Hz)
	ros::Rate loop_rate(1);

	while (nh.ok())
	{
		if(!orderInProgress) {
			connection.updateOrders();
			if(connection.numOrders() >= 1) {
				cout << "ID: " << connection.getCurrentOrder().id << " POS: " << connection.getCurrentOrder().posx << " | " << connection.getCurrentOrder().posy << endl;
				movement.moveTo(connection.getCurrentOrder().posx, connection.getCurrentOrder().posy);
				currentOrderId = connection.getCurrentOrder().id;
				connection.acceptOrder(currentOrderId);
				orderInProgress = true;
			}
		} else {
			if (waiting) {
				if(connection.getOrder(currentOrderId).status >= 3) {
					waiting = false;
					orderInProgress = false;
					movement.moveOnStation();
				} else if (timer >= 60) {
					waiting = false;
					orderInProgress = false;
					connection.finishOrder(currentOrderId);
					movement.moveOnStation();
				}
				timer++;
			} else if (movement.hasArrived()) {
				waiting = true;
				timer = 0;
				connection.setArrived(currentOrderId);
			}
		}
		//publish the assembled command
		ros::spinOnce();

		loop_rate.sleep();
	}
}
