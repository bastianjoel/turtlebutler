//ROS HAUPT BIBLIOTHEK EINBINDEN
#include <ros/ros.h>

//KPBUKI_MSGS BIBLIOTHEKEN EINBINDEN
#include <kobuki_msgs/Led.h>

//KLASSE FÜR LED AUSGEBEN DEFINIEREN
class Blink
{
	//PRIVATER TEIL DER KLASSE (FÜR ANDERE KLASSEN UND FUNKTIONEN
	//UNSICHTBAR)
	private:
		//DER KOMMUNIKATIONSPUNKT DER KLASSE
		ros::NodeHandle _nh;

		//DER "VERÖEFFENTLICHER" (NACHRICHTENAUSGEBER) DER KLASSE
		//WEIL 2 LEDS (2 VERSCHIEDENE ROS TOPICS) 2 PUBLISCHER
		ros::Publisher _ledPub,_ledPub2;
	//OEFFENTLICHER TEIL DER KLASSE (FÜR ANDERE KLASSEN UND FUNKTIONEN
	//SICHTBAR
	public:
		//INTERNE FUNKTION DEFINIEREN
		//IN ABHÄNGIGKEIT DES KOMMUNIKATINOSPUNKTES
		Blink(ros::NodeHandle &nh)
		{
			//DEN KLASSENINTERNEN KOMMUNIKATIONSPUNKT MIT EXTERNEN GLEICH SETZEN
			_nh=nh;

			//VERÖFFENTLICHER AUF BESTIMMTE ROS TOPIC SETZEN
			//MIT NACHRICHTEN DEFINIERUNG UND INTERVALL FÜR NACHFRAGEN DER FUNKTION
			_ledPub= _nh.advertise<kobuki_msgs::Led>("/mobile_base/commands/led1", 1);
			_ledPub2 = _nh.advertise<kobuki_msgs::Led>("/mobile_base/commands/led2", 1);

		}

		//FUNKTION ZUM EXTERNEN AUFRUFEN VON SPÄTEREN ERSTELLTEN OBJEKTEN
		//IN ABHÄNGIGKEIT VON WELCHER LEUCHTFARBE (FARBE), LÄNGE DER LEUCHTDAUER
		// UND LED 1 ODER 2
		//INFOS ZU LED TYPEN :
		//http://docs.ros.org/hydro/api/kobuki_msgs/html/msg/Led.html
		void blink (int type, int _leng, int led) {

			//NACHRICHT DIE SPÄTER GESENDET WIRD ERSTELLEN
			kobuki_msgs::Led blinker;

			//FARBE DES LICHTES DEFINIEREN
			blinker.value=type;

			//SOLANGE ZEIT NOCH NICHT VORBEI
			while (_leng>=0)
			{

				//WENN NICHT LED 1
				if(!led)
				{
					//NACHRICHT AN LED 1 SENDEN
					_ledPub.publish(blinker);

				} else
				{
					//NACHRICHT AN LED 2 SENDEN
					_ledPub2.publish(blinker);
				}

				//FÜR ROS-SYSTEM WIEDERHOLEN
				ros::spinOnce();

				//MAXIMAL ZEIT EINE ABZIEHEN
				_leng--;

			}
		}
};
